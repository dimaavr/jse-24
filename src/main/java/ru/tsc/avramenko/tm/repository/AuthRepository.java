package ru.tsc.avramenko.tm.repository;

import lombok.Getter;
import lombok.Setter;
import ru.tsc.avramenko.tm.api.repository.IAuthRepository;

@Getter
@Setter
public class AuthRepository implements IAuthRepository {

    private String currentUserId;

}